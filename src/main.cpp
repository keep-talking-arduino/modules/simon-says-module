#include <Arduino.h>
#include <SimonSays.h>
#include <Wire.h>

SimonSays simonSays;

void setup(){
  Serial.begin(9600);

  simonSays.setup();

  Serial.println(F("Done loading..."));
}

void loop(){
  simonSays.run();
}
