#include "SimonSays.h"
#include <avr/io.h>
#include <avr/wdt.h>

using namespace Core;

SimonSays::SimonSays()  {
  randomSeed(analogRead(0));
}
SimonSays::~SimonSays() {}

void SimonSays::startModule(){
  CoreModule::startModule();

  /* activate the (four) SimonSays buttons */
  for(int i = 0; i < 4; i++){
    _buttons[i]->activate();
  }

  _flashSequenceTimer = _timer->setTimer(this, 0, 3000);
}

/* stops module and resets values to default values */
void SimonSays::stopModule(){
  CoreModule::stopModule();
  Serial.println(F("stopping..."));
  /* reset values to default values */
  _flashSequenceIndex = 0;
  _pressHistoryIndex = 0;
  for(int i = 0; i < 10; i++){
    _flashSequence[i] = NULL;
    _pressHistory[i] = NULL;
  }

  /* deactivate all (four) SimonSays buttons */
  for(int i = 0; i < 4; i++){
    _buttons[i]->deactivate();
    _buttons[i]->ledOff();
  }
}

/* makes the module reboot entirely */
void SimonSays::resetModule(){
  Serial.println(F("resetting...."));
  CoreModule::resetModule();

  _timer->deleteTimers();
  _shiftHandler->clear();
  _shiftHandler->update();

  for(int i = 0; i < 10; i++){
    _flashSequence[i] = NULL;
  }
  _flashSequenceIndex = 0;

}

/* processes commands received from other modules */
bool SimonSays::processMessage(StackString<TOPIC_SIZE> topic, StackString<PAYLOAD_SIZE> message)
{
  if(CoreModule::processMessage(topic, message)){
    return true;
  };

  // Serial.print(F("Incomming: "));
  // Serial.println(cmd->toString());

  return false;
}

/* generates sequence based on the difficulty given to this module */
void SimonSays::generateModule(){

  /* minimum Difficulty */
  if(_difficulty < 30){
    _difficulty = 30;
  }

  _sequenceLength = _difficulty/10;
  // Serial.print(F("seq: "));

  /* randomly generate sequence of size _difficulty/10 */
  for(int i = 0; i < _sequenceLength; i++){
    _flashSequence[i] = _buttons[random(4)];
    // Serial.print(_flashSequence[i]->getIdentifier());
    // Serial.print(", ");
  }
}

/* defines response to power outage of bomb */
void SimonSays::powerOutage(int duration){
}

/* defines behaviour when user makes an mistake when solving this module */
void SimonSays::processMistake(){
    Serial.println(F("MISTAKE!"));

    /* inform ctrlUnit of mistake */
    _pubSub.publish(TOPIC_BOMB_NOTIFY_STRIKE, _moduleName.c_str()); // TODO remove c_str();

    /* reset press history */
    _pressHistoryIndex = 0;
    for(int i = 0; i < 10; i++){
      _pressHistory[i] = nullptr;
    }

    /* reset sequence so user can start over */
    _flashSequenceIndex = 0;
    stopPlaySequence();

    /* restart sequence */
    _flashSequenceTimer = _timer->setTimer(this, 0, 3000);
}

void SimonSays::stopPlaySequence(){
  _timer->deleteTimers();
  for(int i = 0; i < 4; i++){ // stop currently blinking leds
    _buttons[i]->stopBlink();
  }
}

/* defines behaviour when user solved this module(s repetion) */
void SimonSays::processDisarm(){
  _repetitions--;
  displayRepetitions();
  if(_repetitions <= 0){
    _pubSub.publish(TOPIC_BOMB_NOTIFY_SOLVED, _moduleName.c_str()); // TODO remove c_str();
    stopModule();
    return;
  }

  /* reload for next repetition */
  stopModule();
  generateModule();
  startModule();
}

/* takes currently flashing button as input
and calculates what button the useer shoud press based on this button. */
LedButton* SimonSays::calculateRequiredPress(LedButton* flashingButton){
  LedButton* requiredPress = NULL;

  if (_redBtn->equals(flashingButton)) {
    if(containsVowel(_serialNo)){
      switch(_strikes){
        case 0 : requiredPress = _blueBtn; break;
        case 1 : requiredPress = _yellowBtn; break;
        case 2 : requiredPress = _greenBtn; break;
      }
    }else{
      switch(_strikes){
        case 0 : requiredPress = _blueBtn; break;
        case 1 : requiredPress = _redBtn; break;
        case 2 : requiredPress = _yellowBtn; break;
      }
    }
  }
  else if(flashingButton->equals(_blueBtn)){
    if(containsVowel(_serialNo)){
      switch(_strikes){
        case 0 : requiredPress = _redBtn; break;
        case 1 : requiredPress = _greenBtn; break;
        case 2 : requiredPress = _redBtn; break;
      }
    }else{
      switch(_strikes){
        case 0 : requiredPress = _yellowBtn; break;
        case 1 : requiredPress = _blueBtn; break;
        case 2 : requiredPress = _greenBtn; break;
      }
    }
  }
  else if(flashingButton->equals(_greenBtn)){
    if(containsVowel(_serialNo)){
      switch(_strikes){
        case 0 : requiredPress = _yellowBtn; break;
        case 1 : requiredPress = _blueBtn; break;
        case 2 : requiredPress = _yellowBtn; break;
      }
    }else{
      switch(_strikes){
        case 0 : requiredPress = _greenBtn; break;
        case 1 : requiredPress = _yellowBtn; break;
        case 2 : requiredPress = _blueBtn; break;
      }
    }
  }
  else if(flashingButton->equals(_yellowBtn)){
    if(containsVowel(_serialNo)){
      switch(_strikes){
        case 0 : requiredPress = _greenBtn; break;
        case 1 : requiredPress = _redBtn; break;
        case 2 : requiredPress = _blueBtn; break;
      }
    }else{
      switch(_strikes){
        case 0 : requiredPress = _redBtn; break;
        case 1 : requiredPress = _greenBtn; break;
        case 2 : requiredPress = _redBtn; break;
      }
    }
  }
  else{
    Serial.println(F("NULL"));
  }

  return requiredPress;
}

/* defines behaviour when user presses button */
void SimonSays::onPress(Button* btn){
  stopPlaySequence();
  Serial.print(F(" User pressed: "));
  Serial.println(btn->getIdentifier());

  /* cast to LedButton type */
  LedButton* tmpButton = static_cast<LedButton*>(btn);

  /* If _pressHistory has another button to repeat, user must press that one. */
  if(_pressHistory[_pressHistoryIndex] != NULL){
    /* check if pressed button is the right one */
    if(_pressHistory[_pressHistoryIndex]->equals(tmpButton)){
      Serial.println(F("Repeated history right"));
      _pressHistoryIndex++; // proceed to next history button
    }else{
      Serial.println(F("Repeated history wrong"));
      processMistake();
    }
  }
  else{ // if user already pressed last button in pressHistory
    /* calculate which button user has to press based on currently flashing button */
    LedButton* requiredPress = calculateRequiredPress(_flashSequence[_flashSequenceIndex]);

    Serial.print(F("required press: "));
    Serial.println(requiredPress->getIdentifier());

    /* check if user has pressed the required button, if so, proceed to next step */
    if(tmpButton->equals(requiredPress)){
      _pressHistory[_pressHistoryIndex] = tmpButton; // add pressed button to history
      _pressHistoryIndex = 0; // go back to start of sequence (for user to repeat)

      _flashSequenceIndex++; // increase sequence to repeat

      Serial.println(F("User pressed right"));
      stopPlaySequence();

      /* check if last button of sequence has been pressed */
      if(_flashSequenceIndex >= _sequenceLength) {
        processDisarm();
      }
    }
    else{ // if user pressed button other than the calculated one
      processMistake();
    }
  }

  /* procastinate flashing sequence, only if game is still running
   (game could end inside press method)*/
  if(_running){
    _flashSequenceTimer = _timer->setTimer(this, 0, 3000);
  }


  /* respond to user press by blinking once */
  tmpButton->blinkOnce(800);

}

void SimonSays::onRelease(Button* btn){

}

/* schedule timers to light up sequence of buttons */
void SimonSays::playSequence(){

  long startDelay = 1500;
  for(int i = 0; i <= _flashSequenceIndex; i++){
    if(_flashSequence[i] == NULL){
      break;
    }
    // Serial.print(F("sequence button: "));
    // Serial.println(_flashSequence[i]->getIdentifier());

    if(_flashSequence[i]->equals(_yellowBtn)){
      _yellowBlinkTimer = _timer->setTimer(this, YELLOW_BLINK, startDelay);
    }
    else if(_flashSequence[i]->equals(_greenBtn)){
      _greenBlinkTimer = _timer->setTimer(this, GREEN_BLINK, startDelay);
    }
    else if(_flashSequence[i]->equals(_blueBtn)){
      _blueBlinkTimer = _timer->setTimer(this, BLUE_BLINK, startDelay);
    }
    else if(_flashSequence[i]->equals(_redBtn)){
      _redBlinkTimer = _timer->setTimer(this, RED_BLINK, startDelay);
    }
    /* next button will start blinking 1 second later */
    startDelay += 1500;
  }
  // Serial.println(F("----------------------"));
  // start sequence again in 4 seconds after last one
  _flashSequenceTimer = _timer->setTimer(this, 0, startDelay + 3000);
}

void SimonSays::setup(){
  Serial.println(MODULE_NAME);
  _timer = StensTimer::getInstance();
  CoreModule::setup(I2C_ADDRESS, MODULE_NAME);
  

  _yellowBtn = new LedButton(this, "yellow", A2, A3);
  _greenBtn = new LedButton(this, "green", 9, A0);
  _blueBtn = new LedButton(this, "blue", 7, 8);
  _redBtn = new LedButton(this, "red", 3, 2);

}

void SimonSays::registerModule(){
  CoreModule::registerModule();
  
  _pubSub.subscribe(TOPIC_BOMB_PROPS_SERIAL);
  _pubSub.subscribe(TOPIC_BOMB_PROPS_STRIKES);
}

void SimonSays::run(){
  CoreModule::run();
  _timer->run();

  for(int i = 0; i < 4; i++){// run all buttons
    _buttons[i]->run();
  }
}

void SimonSays::timerCallback(Timer* timer){
  if(!_running){
    return;
  }

  Serial.print(timer->getId());
  if(_flashSequenceTimer->equals(timer)){
    playSequence();
  }else if(YELLOW_BLINK == timer->getAction()){
    _yellowBtn->blinkOnce(800);
  }else if(BLUE_BLINK == timer->getAction()){
    _blueBtn->blinkOnce(800);
  }else if(GREEN_BLINK == timer->getAction()){
    _greenBtn->blinkOnce(800);
  }else if(RED_BLINK == timer->getAction()){
    _redBtn->blinkOnce(800);
  }
}
