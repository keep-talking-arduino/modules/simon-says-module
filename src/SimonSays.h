#ifndef SimonSays_h
#define SimonSays_h

#include <arduino.h>
#include <CoreModule.h>
#include <LedButton.h>
#include <StensTimer.h>

#define I2C_ADDRESS 15
#define MODULE_NAME "SimonSays"

#define YELLOW_BLINK 1
#define GREEN_BLINK 2
#define BLUE_BLINK 3
#define RED_BLINK 4

using namespace Core;

class SimonSays : public CoreModule, public IButtonListener, public IStensTimerListener {

public:
    SimonSays();
    ~SimonSays();

    void setup();
    void registerModule();
    void run();

    void startModule();
    void stopModule();
    void resetModule();
    bool processMessage(StackString<TOPIC_SIZE> topic, StackString<PAYLOAD_SIZE> message);

    void generateModule();
    void powerOutage(int duration);
    void processMistake();
    void processDisarm();
    LedButton* calculateRequiredPress(LedButton* flashingButton);

    void onPress(Button* btn);
    void onRelease(Button* btn);
    void timerCallback(Timer* timer);

    void playSequence();
    void stopPlaySequence();

  private:
    LedButton* _yellowBtn;
    LedButton* _greenBtn;
    LedButton* _blueBtn;
    LedButton* _redBtn;

    StensTimer* _timer;

    LedButton* _buttons[4] = {_greenBtn, _redBtn, _blueBtn, _yellowBtn };
    LedButton* _flashSequence[10] = { NULL };

    int _flashSequenceIndex = 0;
    int _sequenceLength = 0;

    LedButton* _pressHistory[10] = { NULL };
    int _pressHistoryIndex = 0;

    Timer* _flashSequenceTimer;
    Timer* _yellowBlinkTimer;
    Timer* _greenBlinkTimer;
    Timer* _blueBlinkTimer;
    Timer* _redBlinkTimer;
};

#endif
